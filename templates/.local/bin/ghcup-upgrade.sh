#!/usr/bin/env sh
# hls wants a corresponding ghc version - have to upgrade it too.
ghcup upgrade &&
    ghcup install ghc latest &&
    ghcup install hls latest &&
    ghcup set ghc latest &&
    ghcup set hls latest &&
    ghcup install cabal latest &&
    ghcup install stack latest &&
    ghcup set cabal latest &&
    ghcup set stack latest &&
    ghcup gc -opshct &&
    echo Done
