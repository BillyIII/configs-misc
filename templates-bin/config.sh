#!/usr/bin/env sh

# This file is expected to print to stderr and have errors. 
# I don't care as long as it sets the vars that are possible to set.


# Set it to a sane default value. 
# Rely on connection errors in case it isn't valid.
if [ -z "$DISPLAY" ]; then
	export DISPLAY=":0"
fi

if xdpyinfo >/dev/null 2>&1; then
	export HAVE_SCREEN=1
else
	export HAVE_SCREEN=0
fi


if [ $HAVE_SCREEN -gt 0 ]; then
	$(xrandr | awk '/connected primary/ { \
		split($4,g,"+"); \
		split(g[1],r,"x"); \
		print "export SCREEN_WIDTH=" r[1]; \
		print "export SCREEN_HEIGHT=" r[2]; \
		}')

	export FONT_NAME="monospace:style=Regular"
	export FONT_NAME_BOLD="monospace:style=Bold"
	export FONT_NAME_ITALIC="monospace:style=Italic"
	export FONT_NAME_BOLDITALIC="monospace:style=Bold Italic"

    export PANGO_FONT_NAME="monospace"

	if [ $SCREEN_HEIGHT -lt 900 ]; then
		export FONT_SIZE=14
	else
		export FONT_SIZE=20
	fi
fi


if [ `uname` = "Linux" ]; then
	export HAVE_GNU=1
else
	export HAVE_GNU=0
fi
