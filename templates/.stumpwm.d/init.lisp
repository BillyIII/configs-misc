;; -*-lisp-*-
;;
;; Here is a sample .stumpwmrc file

(in-package :stumpwm)

;; change the prefix key to something else
;; (set-prefix-key (kbd "C-t"))

;; prompt the user for an interactive command. The first arg is an
;; optional initial contents.
;; (defcommand colon1 (&optional (initial "")) (:rest)
;;   (let ((cmd (read-one-line (current-screen) ": " :initial-input initial)))
;;     (when cmd
;;       (eval-command cmd t))))

(setq *debug-level* 32000)
(setq *show-command-backtrace* t)
(redirect-all-output (data-dir-file "debug-output" "txt"))

(defcommand doit () ()
  (load "/home/tima/tmp/doit.lisp")
  )

(define-key *root-map* (kbd "d") "doit")
(load "~/quicklisp/setup.lisp")
(require :alexandria)
(require :cl-ppcre)

(set-module-dir "~/src/stumpwm-contrib")

;; Read some doc
;; (define-key *root-map* (kbd "d") "exec gv")
;; Browse somewhere
;; (define-key *root-map* (kbd "b") "colon1 exec firefox http://www.")
;; Ssh somewhere
;; (define-key *root-map* (kbd "C-s") "colon1 exec xterm -e ssh ")

(defun run-if-not-running (pgrep-string command &optional (wait nil))
  "If 'pgrep pgrep-string' fails, run command."
  (when (string= "" (run-shell-command (format nil "pgrep -x ~S" pgrep-string) t))
      (run-shell-command command wait)))

(defcommand screen-windowlist () ()
  (alexandria::if-let ((window-list (mapcan
                                     (lambda (g) (stumpwm::sort-windows (group-windows g)))
                                     (stumpwm::sort-groups (current-screen)))))
    (alexandria::if-let ((window (stumpwm::select-window-from-menu window-list *window-format*)))
      (progn
        (gselect (window-group window))
        (group-focus-window (window-group window) window))
      (throw 'error :abort))
    (message "No Managed Windows")))

(define-stumpwm-type :screen-window-name (input prompt)
  (or (argument-pop input)
      (completing-read (current-screen) prompt
                       (mapcar 'stumpwm::window-name
                               (screen-windows (current-screen))))))

(defcommand screen-select-window (query) ((:screen-window-name "Select: "))
  "Switch to the first window that starts with @var{query}."
  (let (match)
    (labels ((match (win)
               (let* ((wname (stumpwm::window-name win))
                      (end (min (length wname) (length query))))
                 (string-equal wname query :end1 end :end2 end))))
      (unless (null query)
        (setf match (find-if #'match (screen-windows (current-screen)))))
      (when match
        (gselect (window-group match))
        (group-focus-window (window-group match) match)))))

(defcommand short-status () ()
  "Display the short status."
  (message "KEYB ~a~@
            BATT ~a%~@
            TIME ~a"
           (string-trim '(#\space #\linefeed) (run-shell-command "xkb-switch" t))
           (string-trim '(#\space #\linefeed) (run-shell-command "battery" t))
           (time-format "%H:%M%nDATE %e %B %Y, %A")))

(defvar *long-status-conky* nil)

(defun long-status-enter ()
  (setq *long-status-conky* (run-shell-command "conky -c $HOME/.stumpwm.d/long_stats_conky.conf" nil)))

(defun long-status-exit ()
  (sb-ext:process-kill *long-status-conky* 15)
  (setq *long-status-conky* nil))

(define-interactive-keymap long-status (:on-enter #'long-status-enter
                                        :on-exit #'long-status-exit
                                        :exit-on ((kbd "RET")
                                                  (kbd "ESC")
                                                  (kbd "C-g")
                                                  (kbd "SPC")
                                                  (kbd "q"))))

(define-key *root-map* (kbd "l") "exec i3lock-dpms -e")
(define-key *root-map* (kbd "c") "exec st")
(define-key *root-map* (kbd "a") "colon")
(define-key *root-map* (kbd "A") "eval")
(define-key *root-map* (kbd ":") "screen-select-window")
(define-key *root-map* (kbd "C-:") "select-window")
(define-key *root-map* (kbd ";") "screen-windowlist")
(define-key *root-map* (kbd "C-;") "windowlist")
(define-key *root-map* (kbd "'") "short-status")
(define-key *root-map* (kbd "M-'") "long-status")
(define-key *root-map* (kbd "\"") "title")
;;(define-key *root-map* (kbd "s") "pull-hidden-next")
;;(define-key *root-map* (kbd "h") "pull-hidden-previous")
;; (define-key *root-map* (kbd "e") (run-or-raise "emacs" '(:class "Emacs")))
(define-key *root-map* (kbd "e") "exec rofi -show combi -no-lazy-grab")
(define-key *root-map* (kbd "u") "fullscreen")
(define-key *root-map* (kbd "SPC") "mode-line")
(define-key *root-map* (kbd "F20") "short-status")

(define-key *groups-map* (kbd ":") "gselect")
(define-key *groups-map* (kbd ";") "grouplist")

(when (string= (machine-instance) "pcarch")
  (define-key *top-map* (kbd "XF86Tools") "exec stumpwm-xkb-wrap.sh setxkbmap-dv-shifted")
  (define-key *top-map* (kbd "XF86Launch5") "exec stumpwm-xkb-wrap.sh setxkbmap-qw-shifted")
  (define-key *top-map* (kbd "XF86Launch6") "exec stumpwm-xkb-wrap.sh setxkbmap-dv")
  (define-key *top-map* (kbd "XF86Launch7") "exec stumpwm-xkb-wrap.sh setxkbmap-qw"))

(defcommand gselect-or-pull-hidden-next (to-group) (:rest)
  "gselect if not there already, otherwise cycle windows."
  (let ((group-obj (stumpwm::select-group (current-screen) to-group)))
    (if (eq (current-group) group-obj)
        (pull-hidden-next)
      (gselect to-group))))

(define-key *top-map* (kbd "F1") "gselect-or-pull-hidden-next 1")
(define-key *top-map* (kbd "F2") "gselect-or-pull-hidden-next 2")
(define-key *top-map* (kbd "F3") "gselect-or-pull-hidden-next 3")
(define-key *top-map* (kbd "F4") "gselect-or-pull-hidden-next 4")
(define-key *top-map* (kbd "F5") "gselect-or-pull-hidden-next 5")
(define-key *top-map* (kbd "F6") "gselect-or-pull-hidden-next 6")
(define-key *top-map* (kbd "F7") "gselect-or-pull-hidden-next 7")
(define-key *top-map* (kbd "F8") "gselect-or-pull-hidden-next 8")
(define-key *top-map* (kbd "F9") "gselect-or-pull-hidden-next 9")
(define-key *top-map* (kbd "F10") "gselect-or-pull-hidden-next 10")

(define-key *root-map* (kbd "F1") "gmove 1")
(define-key *root-map* (kbd "F2") "gmove 2")
(define-key *root-map* (kbd "F3") "gmove 3")
(define-key *root-map* (kbd "F4") "gmove 4")
(define-key *root-map* (kbd "F5") "gmove 5")
(define-key *root-map* (kbd "F6") "gmove 6")
(define-key *root-map* (kbd "F7") "gmove 7")
(define-key *root-map* (kbd "F8") "gmove 8")
(define-key *root-map* (kbd "F9") "gmove 9")
(define-key *root-map* (kbd "F10") "gmove 10")

(define-key *top-map* (kbd "F11") "fullscreen")
(define-key *top-map* (kbd "Scroll_Lock") "exec toggle-touchpad")
(define-key *top-map* (kbd "SunPrint_Screen") "exec scrot")

;; Redefenition.
(defcommand emacs () ()
  "Start emacs unless it is already running, in which case focus it."
  (run-or-raise "ecgui" '(:class "Emacs")))

;; Web jump (works for Google and Imdb)
(defmacro make-web-jump (name prefix)
  `(defcommand ,(intern name) (search) ((:rest ,(concatenate 'string name " search: ")))
    (substitute #\+ #\Space search)
    (run-shell-command (concatenate 'string ,prefix search))))

(setq *mode-line-background-color* "Black")
(setq *mode-line-foreground-color* "White")
(setq *mode-line-border-color* "Gray30")

;; (defparameter *panel-command* "lxpanel")

;; (defcommand restart-panel () ()
;;   "Restart the modeline panel"
;;   (run-shell-command (concatenate 'string "killall " *panel-command*) t)
;;   (run-shell-command *panel-command*)
;;   (run-with-timer 2 nil #'(lambda () (mode-line) (mode-line))))

;; (make-web-jump "google" "firefox http://www.google.fr/search?q=")
;; (make-web-jump "imdb" "firefox http://www.imdb.com/find?q=")

;; C-t M-s is a terrble binding, but you get the idea.
;; (define-key *root-map* (kbd "M-s") "google")
;; (define-key *root-map* (kbd "i") "imdb")

;; Message window font
;; (set-font "-xos4-terminus-medium-r-normal--14-140-72-72-c-80-iso8859-15")
;; (ql:quickload :clx-truetype)
;; (load-module "ttf-fonts")
;; (xft:cache-fonts)
;; (set-font (make-instance 'xft:font :family "DejaVu Serif" :subfamily "Book" :size 14))
;; (set-font (make-instance 'xft:font :family "DejaVu Serif" :subfamily "Regular" :size 14))

(setf *deny-raise-request* t)

;;; Define window placement policy...

;; Clear rules
(clear-window-placement-rules)

;; Last rule to match takes precedence!
;; TIP: if the argument to :title or :role begins with an ellipsis, a substring
;; match is performed.
;; TIP: if the :create flag is set then a missing group will be created and
;; restored from *data-dir*/create file.
;; TIP: if the :restore flag is set then group dump is restored even for an
;; existing group using *data-dir*/restore file.
;; (define-frame-preference "Default"
;;   ;; frame raise lock (lock AND raise == jumpto)
;;   (0 t nil :class "Konqueror" :role "...konqueror-mainwindow")
;;   (1 t nil :class "XTerm"))

;; (define-frame-preference "Ardour"
;;   (0 t   t   :instance "ardour_editor" :type :normal)
;;   (0 t   t   :title "Ardour - Session Control")
;;   (0 nil nil :class "XTerm")
;;   (1 t   nil :type :normal)
;;   (1 t   t   :instance "ardour_mixer")
;;   (2 t   t   :instance "jvmetro")
;;   (1 t   t   :instance "qjackctl")
;;   (3 t   t   :instance "qjackctl" :role "qjackctlMainForm"))

;; (define-frame-preference "Shareland"
;;   (0 t   nil :class "XTerm")
;;   (1 nil t   :class "aMule"))

;; (define-frame-preference "Emacs"
;;   (1 t t :restore "emacs-editing-dump" :title "...xdvi")
;;   (0 t t :create "emacs-dump" :class "Emacs"))

(case (machine-instance)
  ("pcarch" (run-shell-command "stumpwm-xkb-wrap.sh setxkbmap-dv-shifted" t))
  ("freebsd" (run-shell-command "stumpwm-xkb-wrap.sh setxkbmap-dv-shifted-freebsd" t))
  ("tima-laptop" (run-shell-command "stumpwm-xkb-wrap.sh setxkbmap-dv" t))
  (otherwise (run-shell-command "stumpwm-xkb-fix.sh" t)))

;; (if (string= (machine-instance) "pcarch")
;;    (run-shell-command "stumpwm-xkb-wrap.sh setxkbmap-dv-shifted" t)
;; (run-shell-command "stumpwm-xkb-wrap.sh setxkbmap-dv-shifted-freebsd" t)
;; (run-shell-command "stumpwm-xkb-fix.sh" t)

(set-prefix-key (kbd "F20"))

(run-shell-command "session-on-init")
;; (run-shell-command "gui-session-on-init" t)

(run-if-not-running "picom" "picom -b" t)
(run-shell-command "hsetroot -solid #000000")
(run-if-not-running "unclutter" "unclutter -jitter 10")

;; (run-if-not-running "xfce4-panel" "xfce4-panel")
;; (run-if-not-running "fbpanel" "fbpanel")
;; (run-if-not-running "lxpanel" "lxpanel")
;; (mode-line)
;; (restart-panel)

(setf (group-name (current-group)) "terminal")
(define-frame-preference "terminal"
  (0 t t :class "st-256color"))

(add-group (current-screen) "emacs")
(define-frame-preference "emacs"
  (0 t t :class "Emacs"))

(add-group (current-screen) "browser")
(define-frame-preference "browser"
  (0 t t :class "firefox"))

(add-group (current-screen) "video")
(define-frame-preference "video"
  (0 t t :class "mpv"))

(add-group (current-screen) "misc")

(add-group (current-screen) "games")
(define-frame-preference "games"
  (0 t t :class "Steam"))

(add-group (current-screen) "VII")
(add-group (current-screen) "VIII")
(add-group (current-screen) "IX")
(add-group (current-screen) "X")

;; (add-group (current-screen) ".mpv" :type 'float-group)
;; (define-frame-preference ".mpv"
  ;; (0 t t :class "mpv"))


(run-if-not-running "st" "st -e term-session-on-init")
;;(run-shell-command "xterm")
;; ecgui still runs on a wm restart.
;; Maybe it's not managing/considering pre-existing windows until it's fully initialized?
;; (run-or-raise "ecgui" '(:class "Emacs"))
;;(run-if-not-running "palemoon" "palemoon")
;;(run-if-not-running "firefox" "firefox")
