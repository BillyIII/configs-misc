#!/usr/bin/env sh

BINROOT="`dirname $0`"

# Execute before the `set -e` statement.
. "$BINROOT/config.sh"

set -eu


CLOBBER=no
DRYRUN=no
UPDATE=no

while getopts 'fnu' OPT; do
	case "$OPT" in
		f) CLOBBER=yes;;
		n) DRYRUN=yes;;
		u) UPDATE=yes;;
		'?') exit 1;;
	esac
done
shift $(( OPTIND - 1 ))

TEMPLROOT=${1%%"/"}
FILEROOT=${2%%"/"}
LINKROOT=${3%%"/"}
shift 3

if [ "$DRYRUN" = yes ]; then
    CMDPFX=echo
else
    CMDPFX=
fi


die() {
    echo 1>&2 "Error while processing $TEMPLPATH"
    echo 1>&2 $*
    exit 1
}

transform_name() {
    local NAME LINES
    NAME=`echo "$1" | $BINROOT/esh -`

    LINES=`echo "$NAME" | wc -l`
    if [ $LINES -gt 1 ]; then
        NAME=${NAME@Q}
        NAME=${NAME:1}
        die "Multiline filename after expansion: $NAME"
    fi

    RETVAL=$NAME
}

mk_file() {
    local TEMPLPATH FILEPATH
    TEMPLPATH=$1
    FILEPATH=$FILEROOT/$2

    if [ "$CLOBBER" != yes ]; then
        if [ -e "$FILEPATH" -a -w "$FILEPATH" ]; then
            die "Output file exists and is writable: $FILEPATH"
        fi
    fi

    if [ "$UPDATE" = no -o "$FILEPATH" -ot "$TEMPLPATH" ]; then
        $CMDPFX rm -f "$FILEPATH"
        $CMDPFX mkdir -p `dirname "$FILEPATH"`
        $CMDPFX $BINROOT/esh -o "$FILEPATH" "$TEMPLPATH"
        $CMDPFX chmod -w "$FILEPATH"

        if [ -x "$TEMPLPATH" ]; then
            $CMDPFX chmod u+x "$FILEPATH"
        fi    
    fi

    RETVAL=$FILEPATH
}

mk_link() {
    local FILEPATH LINKPATH
    FILEPATH=$1
    LINKPATH=$LINKROOT/$2

    if [ "$CLOBBER" != yes ]; then
        if [ -e "$LINKPATH" -a ! -h "$LINKPATH" ]; then
            die "Link file exists and is not a link: $LINKPATH"
        fi
    fi

    if [ "$UPDATE" = no -o "$LINKPATH" -ot "$FILEPATH" ]; then
        $CMDPFX rm -f "$LINKPATH"
        $CMDPFX mkdir -p `dirname "$LINKPATH"`
        $CMDPFX ln -s "$FILEPATH" "$LINKPATH"
    fi

    RETVAL=$LINKPATH
}

do_file() {
    local TEMPLPATH RELPATH
    TEMPLPATH=$1
    # Substitution is not necessary, but it makes it prettier.
    RELPATH=${2#"/"}

    echo Processing $RELPATH
    mk_file "$TEMPLPATH" "$RELPATH"
    mk_link "$RETVAL" "$RELPATH"
}

do_child() {
    local TEMPLPATH RELPATH NAME CHRELPATH
    TEMPLPATH=$1
    RELPATH=$2

    NAME=`basename "$TEMPLPATH"`
    # Filter out empty wildcard results.
    if [ "$NAME" != "." -a "$NAME" != ".." -a "$NAME" != "*" -a "$NAME" != ".*" ]; then
        transform_name "$NAME"
        # Skip empty expansions.
        if [ -n "$RETVAL" ]; then
            CHRELPATH=$RELPATH/$RETVAL
            if [ -d "$TEMPLPATH" ]; then
                do_dir "$TEMPLPATH" "$CHRELPATH"
            elif [ -f "$TEMPLPATH" ]; then
                do_file "$TEMPLPATH" "$CHRELPATH"
            else
                die "Invalid file type: $TEMPLPATH"
            fi
        fi
    fi
}

do_dir() {
    local TEMPLPATH RELPATH CHILD
    TEMPLPATH=$1
    RELPATH=$2

    # Yaay, portability and relative reliability.
    for CHILD in $TEMPLPATH/*; do
        do_child "$CHILD" "$RELPATH"
    done

    # Ditto.
    for CHILD in $TEMPLPATH/.*; do
        do_child "$CHILD" "$RELPATH"
    done
}

echo ROOT = $TEMPLROOT
echo CLOBBER = $CLOBBER
echo DRYRUN = $DRYRUN
echo UPDATE = $UPDATE

do_dir $TEMPLROOT ""
