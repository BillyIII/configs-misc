local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

local glib = require("lgi").GLib
local DateTime = glib.DateTime
local TimeZone = glib.TimeZone

local pttnotif = {}


function pttnotif.init()
   local pttnotif_box = wibox({
         ontop = true,
         visible = false,
         x = 0,
         y = 0,
         width = 1,
         height = 1,
         opacity = 1.0,
         bg = beautiful.bg_focus,
         fg = beautiful.fg_focus,
         shape = gears.shape.round_rect,
         type = "toolbar"
   })

   pttnotif_box:setup({
         {
            id = "text", 
            align = "left",
            font = beautiful.font,
            widget = wibox.widget.textbox
         },
         id = "margin",
         margins = beautiful.border_width,
         color = beautiful.border_focus,
         layout = wibox.container.margin,
   })

   awful.screen.connect_for_each_screen(function(s)
         s.pttnotif_box = pttnotif_box
   end)
end

function pttnotif.show()
   local screen = mouse.screen;
   local box = screen.pttnotif_box
   local margin = box:get_children_by_id("margin")[1]
   local text = box:get_children_by_id("text")[1]
   
   box.screen = screen

   text:set_markup("<b>TALKING</b>")

   local width, height = text:get_preferred_size(s)
   -- (*1.1): Width is not always correct. Avoid clipping on line wrap.
   box.width = width * 1.1 + margin.left + margin.right
   box.height = height + margin.top + margin.bottom
   awful.placement.align(
      box,
      {
         position = "top",
         honor_padding = true,
         honor_workarea = true,
      }
   )

   box.visible = true
end

function pttnotif.hide()
   screen[1].pttnotif_box.visible = false
end

return pttnotif
