#!/usr/bin/env sh
set -eu

ROOT="$HOME/pictures/camera"
CAMROOT="/store_00010001/DCIM"

mkdir -p "$ROOT"
cd "$ROOT"

exec gphoto2 --get-all-files --skip-existing --filename '%F/%:' --folder "$CAMROOT"
