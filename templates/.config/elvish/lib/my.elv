
fn last-command { edit:command-history &cmd-only &newest-first | take 1 }

# var last-command-failed = $false

# set edit:after-command = (conj $edit:after-command { |a| set last-command-failed = (not-eq $nil $a[error]) })

fn prompt {
  # put (styled (whoami) green)'@'(styled (hostname) red)' '
  # styled '['(last-command)']' (if $last-command-failed { put blue inverse } else { put blue })
  # put ' '(tilde-abbr $pwd)"\n"
  put (styled (date +%T) blue)' '(styled (tilde-abbr $pwd)"\n" cyan)
}

fn rprompt {
  put (styled (whoami) green)'@'(styled (hostname) red)
}

var window-title-max-length = 12

fn format-window-title {|str|
  var len = (count $str)
  if (<= $len $window-title-max-length) {
    put $str
  } else {
    # -1 is for the ellipsis char
    put '…'$str[(- $len $window-title-max-length -1)..]
  }
}
