-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
   naughty.notify({ preset = naughty.config.presets.critical,
                    title = "Oops, there were errors during startup!",
                    text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
   local in_error = false
   awesome.connect_signal("debug::error", function (err)
                             -- Make sure we don't go into an endless error loop
                             if in_error then return end
                             in_error = true

                             naughty.notify({ preset = naughty.config.presets.critical,
                                              title = "Oops, an error happened!",
                                              text = tostring(err) })
                             in_error = false
   end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "st"
editor = os.getenv("EDITOR") or "mg"
editor_cmd = terminal .. " -e " .. editor
-- editor_cmd = editor

-- beautiful.master_width_factor = 0.75

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
   awful.layout.suit.max,
   awful.layout.suit.tile,
   -- awful.layout.suit.floating,
   -- awful.layout.suit.tile.left,
   -- awful.layout.suit.tile.bottom,
   -- awful.layout.suit.tile.top,
   -- awful.layout.suit.fair,
   -- awful.layout.suit.fair.horizontal,
   -- awful.layout.suit.spiral,
   -- awful.layout.suit.spiral.dwindle,
   -- awful.layout.suit.max.fullscreen,
   -- awful.layout.suit.magnifier,
   -- awful.layout.suit.corner.nw,
   -- awful.layout.suit.corner.ne,
   -- awful.layout.suit.corner.sw,
   -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                             { "open terminal", terminal }
}
                       })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
   awful.button({ }, 1, function(t) t:view_only() end),
   awful.button({ modkey }, 1, function(t)
         if client.focus then
            client.focus:move_to_tag(t)
         end
   end),
   awful.button({ }, 3, awful.tag.viewtoggle),
   awful.button({ modkey }, 3, function(t)
         if client.focus then
            client.focus:toggle_tag(t)
         end
   end),
   awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
   awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
   awful.button({ }, 1, function (c)
         if c == client.focus then
            c.minimized = true
         else
            c:emit_signal(
               "request::activate",
               "tasklist",
               {raise = true}
            )
         end
   end),
   awful.button({ }, 3, function()
         awful.menu.client_list({ theme = { width = 250 } })
   end),
   awful.button({ }, 4, function ()
         awful.client.focus.byidx(1)
   end),
   awful.button({ }, 5, function ()
         awful.client.focus.byidx(-1)
end))

local function set_wallpaper(s)
   -- Wallpaper
   if beautiful.wallpaper then
      local wallpaper = beautiful.wallpaper
      -- If wallpaper is a function, call it with the screen
      if type(wallpaper) == "function" then
         wallpaper = wallpaper(s)
      end
      gears.wallpaper.maximized(wallpaper, s, true)
      -- gears.wallpaper.fit(wallpaper, s)
   end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
      -- Wallpaper
      set_wallpaper(s)

      -- Each screen has its own tag table.
      local tags = nil
      if s == awful.screen.primary then
         tags = { "terminal", "emacs", "browser", "video", "misc", "games", "7", "8", "9" }
      else
         tags = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }
      end
      
      awful.tag(tags, s, awful.layout.layouts[1])

      -- Create a promptbox for each screen
      s.mypromptbox = awful.widget.prompt()
      -- Create an imagebox widget which will contain an icon indicating which layout we're using.
      -- We need one layoutbox per screen.
      s.mylayoutbox = awful.widget.layoutbox(s)
      s.mylayoutbox:buttons(gears.table.join(
                               awful.button({ }, 1, function () awful.layout.inc( 1) end),
                               awful.button({ }, 3, function () awful.layout.inc(-1) end),
                               awful.button({ }, 4, function () awful.layout.inc( 1) end),
                               awful.button({ }, 5, function () awful.layout.inc(-1) end)))
      -- Create a taglist widget
      s.mytaglist = awful.widget.taglist {
         screen  = s,
         filter  = awful.widget.taglist.filter.all,
         buttons = taglist_buttons
      }

      -- Create a tasklist widget
      s.mytasklist = awful.widget.tasklist {
         screen  = s,
         filter  = awful.widget.tasklist.filter.currenttags,
         buttons = tasklist_buttons
      }

      -- Create the wibox
      s.topbar = awful.wibox({ position = "top", stretch = true, screen = s, visible = false, ontop = true, type = "toolbar" })

      -- Add widgets to the wibox
      s.topbar:setup {
         layout = wibox.layout.align.horizontal,
         { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
         },
         s.mytasklist, -- Middle widget
         { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
         },
      }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
                awful.button({ }, 3, function () mymainmenu:toggle() end),
                awful.button({ }, 4, awful.tag.viewnext),
                awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}


local modalbind = require("modalbind")
modalbind.init()

local shortstatus = require("shortstatus")
shortstatus.init()

local revelation=require("revelation")
revelation.charorder = "uhetonas.cpg,r;lkbjmqw'v'z",
revelation.init()

local pttnotif = require("pttnotif")
pttnotif.init()


function set_wibox_visibility(v)
   for s in screen do
      s.topbar.visible = v
   end
end

function show_wibox()
   set_wibox_visibility(true)
end

function hide_wibox()
   set_wibox_visibility(false)
end

function toggle_wibox()
   for s in screen do
      s.topbar.visible = not s.topbar.visible
   end
end

function switch_tag_or_client(num)
   local screen = awful.screen.focused()
   local tag = screen.tags[num]
   if tag then
      if tag.selected then
         awful.client.focus.byidx(1)
      else
         tag:view_only()
      end
   end
end

-- Automatically restore self-minimizing "fullscreen" games.
tag.connect_signal("property::selected", function()
                      local t = awful.tag.selected()
                      if t then
                         -- Find the first and only client.
                         local c = nil
                         local single = true
                         for k,v in pairs(t:clients()) do
                            if c and v then
                               single = false
                               break
                            end
                            c = v
                         end
                         -- If there is only one client and it's minimized, restore it.
                         if single and c and c.minimized then
                            c.minimized = false
                            c:emit_signal(
                               "request::activate", "key.unminimize", {raise = true}
                            )
                         end
                      end
end)

modalbind.default_keys = {
   -- { "separator", "" },
   { "Escape",   modalbind.close_box, nil },
   { "g",        modalbind.close_box, nil },
   { "Super_L",  modalbind.close_box, nil },
}

local quit_keymap = {
   { "q", modalbind.close_box, nil },
   -- { "separator", "    <b><u>Confirm</u></b>" },
   { "r", awesome.restart, "Restart" },
   { "y", awesome.quit, "Quit" },
}

local layout_keymap = {
   -- { "separator", "    <b><u>Layout</u></b>" },
   { "l",     function () awful.tag.incmwfact( 0.05)          end, "Increase master width factor" },
   { "h",     function () awful.tag.incmwfact(-0.05)          end, "Decrease master width factor" },
   { "H",     function () awful.tag.incnmaster( 1, nil, true) end, "Increase the number of master clients" },
   { "L",     function () awful.tag.incnmaster(-1, nil, true) end, "Decrease the number of master clients" },
   { "c",     function () awful.tag.incncol( 1, nil, true)    end, "Increase the number of columns" },
   { "C",     function () awful.tag.incncol(-1, nil, true)    end, "Decrease the number of columns" },
   -- { "space", function () awful.layout.inc( 1)                end, "Select next" },
   { "n", function () awful.layout.inc( 1)                end, "Select next" },
   { "p", function () awful.layout.inc(-1)                end, "Select previous" },
}

local last_english_layout = 0
local normal_mode_layout = nil

local command_keymap = {
   { "onOpen", function ()
        normal_mode_layout = awesome.xkb_get_layout_group()
        -- Restore the last seen english layout on close.
        if normal_mode_layout == 2 then
           normal_mode_layout = last_english_layout
        else
           last_english_layout = normal_mode_layout
        end
		awesome.xkb_set_layout_group(1)
        shortstatus.show()
   end },
   { "onClose", function ()
		awesome.xkb_set_layout_group(normal_mode_layout)
        shortstatus.hide()
   end },
   { "separator", "    <b><u>Misc.</u></b>" },
   { "c", function () awful.spawn(terminal) end,
     "Terminal" },
   { "e", function() menubar.show() end,
     "Menu" },
   { "m", toggle_wibox, "Toggle topbar" },
   { "M", hide_wibox, "Hide topbar" },
   -- { "x", revelation.expose({curr_tag_only=true}) , "Expose" },
   { "N", function() awful.screen.focus_relative(1) end,
     "Next screen" },
   { "P", function() awful.screen.focus_relative(-1) end,
     "Prev screen" }, 
   { "q", function() modalbind.grab{keymap=quit_keymap, name="Quit mode", stay_in_mode=false} end,
     "Quit" },
   { "separator", "    <b><u>Client</u></b>" },
   { "n", function() awful.client.focus.byidx( 1) end,
     "Next" },
   { "p", function() awful.client.focus.byidx(-1) end,
     "Prev" }, 
   { "o", function() local c = client.focus; c:move_to_screen() end,
     "Move to screen" },
   { "Return", function() client.focus:swap(awful.client.getmaster()) end,
     "Zoom" },
   { "k", function() client.focus:kill() end,
     "Kill" }, 
   { "f", function() awful.client.floating.toggle(client.focus) end,
     "Float" }, 
   { "u", function() local c = client.focus; c.fullscreen = not c.fullscreen; c:raise() end,
     "Fullscreen" },
   { "t", function() local c = client.focus; c.ontop = not c.ontop end,
     "On Top" },
   { "s", function() local c = client.focus; c.sticky = not c.sticky end,
     "Sticky" },
   { "R",
     function ()
        local c = awful.client.restore()
        -- Focus restored client
        if c then
           c:emit_signal(
              "request::activate", "key.unminimize", {raise = true}
           )
        end
     end,
     "Restore" },
   { "x", function() awful.spawn("emacsclient --eval \"(emacs-everywhere)\"") end,
     "Edit in Emacs" },
   { "separator", "    <b><u>Layout</u></b>" },
   { "l", function () awful.layout.inc( 1) end,
     "Next" },
   { "h", function () awful.layout.inc( -1) end,
     "Prev" },
   { "r", function() modalbind.grab{keymap=layout_keymap, name="Layout mode", stay_in_mode=true} end,
     "More" },
   { "separator", "    <b><u>Keyboard</u></b>" },
   { "&", function () normal_mode_layout = 1 end,
     "Programmer Dvorak" },
   { "[", function () normal_mode_layout = 2 end,
     "Йцукен" },
   { "{", function () normal_mode_layout = 0 end,
     "Qwerty" },
}


-- {{{ Key bindings
globalkeys = gears.table.join(
   awful.key({}, "Super_L", function() modalbind.grab{keymap=command_keymap, name="Command mode", stay_in_mode=false} end,
      {description="enter command mode", group="awesome"}),
   awful.key({}, "Scroll_Lock", function() awful.spawn("toggle-touchpad") end,
      {description="toggle the touchpad", group="awesome"}),
   awful.key({}, "Print", function() awful.spawn("printscreen-window") end,
      {description="scrot", group="awesome"}),
   awful.key({ "Mod1" }, "Print", function() awful.spawn("printscreen") end,
      {description="scrot -u", group="awesome"}),
   awful.key({ "Control" }, "Print", function() awful.spawn("printscreen-area") end,
      {description="scrot -s", group="awesome"}),
   awful.key({ "Shift" }, "Super_L", function () awful.screen.focus_relative( 1) end,
             {description = "focus the next screen", group = "screen"}),
   awful.key({ "Control" }, "Super_L", function () awful.screen.focus_relative(-1) end,
             {description = "focus the previous screen", group = "screen"}),
   awful.key({ "Mod1" }, "Escape", function() revelation.expose({curr_tag_only=true}) end,
      {description="Expose", group="awesome"})
   
   -- awful.key({}, "XF86Tools",
   --    function()
   --       pttnotif.show()
   --       awful.spawn("mic-unmute")
   --    end,
   --    function()
   --       awful.spawn("mic-mute")
   --       pttnotif.hide()
   --    end,
   --    {description="Push-to-talk", group="awesome"})
   
   -- awful.key({ "Control" }, "Shift_L", function () awful.screen.focus_relative(1) end,
             -- {description = "focus the next screen", group = "screen"}),
   -- awful.key({ "Shift" }, "Control_L", function () awful.screen.focus_relative(1) end,
             -- {description = "focus the next screen", group = "screen"})
   -- awful.key({}, "#133", function() modalbind.grab{keymap=command_keymap, name="Command", stay_in_mode=false}  end,
   -- {description="enter command mode", group="awesome"})
   -- awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
   --           {description="show help", group="awesome"}),
   -- awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
   --           {description = "view previous", group = "tag"}),
   -- awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
   --           {description = "view next", group = "tag"}),
   -- awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
   --           {description = "go back", group = "tag"}),

   -- awful.key({ modkey,           }, "j",
   --     function ()
   --         awful.client.focus.byidx( 1)
   --     end,
   --     {description = "focus next by index", group = "client"}
   -- ),
   -- awful.key({ modkey,           }, "k",
   --     function ()
   --         awful.client.focus.byidx(-1)
   --     end,
   --     {description = "focus previous by index", group = "client"}
   -- ),
   -- awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
   --           {description = "show main menu", group = "awesome"}),

   -- -- Layout manipulation
   -- awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
   --           {description = "swap with next client by index", group = "client"}),
   -- awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
   --           {description = "swap with previous client by index", group = "client"}),
   -- awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
   --           {description = "focus the next screen", group = "screen"}),
   -- awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
   --           {description = "focus the previous screen", group = "screen"}),
   -- awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
   --           {description = "jump to urgent client", group = "client"}),
   -- awful.key({ modkey,           }, "Tab",
   --     function ()
   --         awful.client.focus.history.previous()
   --         if client.focus then
   --             client.focus:raise()
   --         end
   --     end,
   --     {description = "go back", group = "client"}),

   -- -- Standard program
   -- awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
   --           {description = "open a terminal", group = "launcher"}),
   -- awful.key({ modkey, "Control" }, "r", awesome.restart,
   --           {description = "reload awesome", group = "awesome"}),
   -- awful.key({ modkey, "Shift"   }, "q", awesome.quit,
   --           {description = "quit awesome", group = "awesome"}),

   -- awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
   --           {description = "increase master width factor", group = "layout"}),
   -- awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
   --           {description = "decrease master width factor", group = "layout"}),
   -- awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
   --           {description = "increase the number of master clients", group = "layout"}),
   -- awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
   --           {description = "decrease the number of master clients", group = "layout"}),
   -- awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
   --           {description = "increase the number of columns", group = "layout"}),
   -- awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
   --           {description = "decrease the number of columns", group = "layout"}),
   -- awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
   --           {description = "select next", group = "layout"}),
   -- awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
   --           {description = "select previous", group = "layout"}),

   -- awful.key({ modkey, "Control" }, "n",
   --           function ()
   --               local c = awful.client.restore()
   --               -- Focus restored client
   --               if c then
   --                 c:emit_signal(
   --                     "request::activate", "key.unminimize", {raise = true}
   --                 )
   --               end
   --           end,
   --           {description = "restore minimized", group = "client"}),

   -- -- Prompt
   -- awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
   --           {description = "run prompt", group = "launcher"}),

   -- awful.key({ modkey }, "x",
   --           function ()
   --               awful.prompt.run {
   --                 prompt       = "Run Lua code: ",
   --                 textbox      = awful.screen.focused().mypromptbox.widget,
   --                 exe_callback = awful.util.eval,
   --                 history_path = awful.util.get_cache_dir() .. "/history_eval"
   --               }
   --           end,
   --           {description = "lua execute prompt", group = "awesome"}),
   -- -- Menubar
   -- awful.key({ modkey }, "p", function() menubar.show() end,
   --           {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(
   -- awful.key({ modkey,           }, "f",
   --     function (c)
   --         c.fullscreen = not c.fullscreen
   --         c:raise()
   --     end,
   --     {description = "toggle fullscreen", group = "client"}),
   -- awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
   --           {description = "close", group = "client"}),
   -- awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
   --           {description = "toggle floating", group = "client"}),
   -- awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
   --           {description = "move to master", group = "client"}),
   -- awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
   --           {description = "move to screen", group = "client"}),
   -- awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
   --           {description = "toggle keep on top", group = "client"}),
   -- awful.key({ modkey,           }, "n",
   --     function (c)
   --         -- The client currently has the input focus, so it cannot be
   --         -- minimized, since minimized clients can't have the focus.
   --         c.minimized = true
   --     end ,
   --     {description = "minimize", group = "client"}),
   -- awful.key({ modkey,           }, "m",
   --     function (c)
   --         c.maximized = not c.maximized
   --         c:raise()
   --     end ,
   --     {description = "(un)maximize", group = "client"}),
   -- awful.key({ modkey, "Control" }, "m",
   --     function (c)
   --         c.maximized_vertical = not c.maximized_vertical
   --         c:raise()
   --     end ,
   --     {description = "(un)maximize vertically", group = "client"}),
   -- awful.key({ modkey, "Shift"   }, "m",
   --     function (c)
   --         c.maximized_horizontal = not c.maximized_horizontal
   --         c:raise()
   --     end ,
   --     {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
   command_keymap = gears.table.join(
      command_keymap,
      {{ "F" .. i, function()
            if client.focus then
               local tag = client.focus.screen.tags[i]
               if tag then
                  client.focus:move_to_tag(tag)
               end
            end
      end, nil }});
   globalkeys = gears.table.join(
      globalkeys,
      -- View tag only.
      awful.key({}, "F" .. i,
         function ()
            switch_tag_or_client(i)
         end,
         {description = "view tag #"..i, group = "tag"}),
      -- Toggle tag display.
      awful.key({ "Control" }, "F" .. i,
         function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
               awful.tag.viewtoggle(tag)
            end
         end,
         {description = "toggle tag #" .. i, group = "tag"}),
      -- Move client to tag.
      awful.key({ "Shift" }, "F" .. i,
         function ()
            if client.focus then
               local tag = client.focus.screen.tags[i]
               if tag then
                  client.focus:move_to_tag(tag)
               end
            end
         end,
         {description = "move focused client to tag #"..i, group = "tag"}),
      -- Toggle tag on focused client.
      awful.key({ "Control", "Shift" }, "F" .. i,
         function ()
            if client.focus then
               local tag = client.focus.screen.tags[i]
               if tag then
                  client.focus:toggle_tag(tag)
               end
            end
         end,
         {description = "toggle focused client on tag #" .. i, group = "tag"})
   )
end

clientbuttons = gears.table.join(
   awful.button({ }, 1, function (c)
         c:emit_signal("request::activate", "mouse_click", {raise = true})
   end),
   awful.button({ modkey }, 1, function (c)
         c:emit_signal("request::activate", "mouse_click", {raise = true})
         awful.mouse.client.move(c)
   end),
   awful.button({ modkey }, 3, function (c)
         c:emit_signal("request::activate", "mouse_click", {raise = true})
         awful.mouse.client.resize(c)
   end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
   -- All clients will match this rule.
   { rule = { },
     properties = { border_width = 0,
                    border_color = 0,
                    -- properties = { border_width = beautiful.border_width,
                    -- border_color = beautiful.border_normal,
                    focus = awful.client.focus.filter,
                    raise = true,
                    keys = clientkeys,
                    buttons = clientbuttons,
                    screen = awful.screen.preferred,
                    placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                    -- floating = false,
     }
   },

   -- Floating clients.
   { rule_any = {
        instance = {
           "DTA",  -- Firefox addon DownThemAll.
           "copyq",  -- Includes session name in class.
           "pinentry",
        },
        class = {
           "Arandr",
           "Blueman-manager",
           "Gpick",
           "Kruler",
           "MessageWin",  -- kalarm.
           "Sxiv",
           "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
           "Wpa_gui",
           "veromix",
           "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
           "Event Tester",  -- xev.
        },
        role = {
           "AlarmWindow",  -- Thunderbird's calendar.
           "ConfigManager",  -- Thunderbird's about:config.
           "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
   }, properties = { floating = true }},

   -- Add titlebars to normal clients and dialogs
   -- { rule_any = {type = { "normal", "dialog" }
   -- }, properties = { titlebars_enabled = true }
   -- },

   -- Set Firefox to always map on the tag named "2" on screen 1.
   -- { rule = { class = "Firefox" },
   --   properties = { screen = 1, tag = "2" } },
   { rule = { class = "mpv" },
     properties =
        { floating = true,
          ontop = true,
          sticky = true,
          placement = awful.placement.bottom_right } },
   { rule = { class = "gram.exe" },
     properties =
        { floating = true,
          ontop = true,
          placement = awful.placement.top_right,
          -- maximized_horizontal = true,
          screen = 2, tag = "1"} },
   { rule_any = { class = { "steam", "Steam" } },
     properties =
        { screen = 1, tag = "6", focus = false } },
   -- steam tends to gain exclusive (wth xorg?!!) keyboard focus
   -- this *might* have fixed it
   -- potentially related:
   -- https://github.com/ValveSoftware/steam-for-linux/issues/9458
   { rule = { class = "steamwebhelper" },
     properties =
        { screen = 1, tag = "6", focus = false } },
   { rule_any = { class = { "telegram-desktop", "TelegramDesktop" } },
     properties =
        { screen = 1, tag = "5"} },
   { rule = { class = "discord" },
     properties =
        { screen = 1, tag = "5"} },
   { rule = { class = "chromium" },
     properties =
        { screen = 1, tag = "5"} },
   { rule_any = { class = { "firefox", "Firefox" } },
     properties =
        { fullscreen = true } },
   { rule = { icon_name = "xzoom" },
     properties =
        { floating = true } },
   { rule = { class = "Conky" },
     properties =
        { screen = 2, tag = "2"} },
   -- { rule = { class = "Zathura" },
   --   properties =
   --      { fullscreen = true } },
   -- { rule = { class = "vmg" },
   --   properties =
   --      { floating = true } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal(
   "manage", function (c)
      -- Set the windows at the slave,
      -- i.e. put it at the end of others instead of setting it master.
      -- if not awesome.startup then awful.client.setslave(c) end

      if awesome.startup
         and not c.size_hints.user_position
         and not c.size_hints.program_position then
         -- Prevent clients from being unreachable after screen count changes.
         awful.placement.no_offscreen(c)
      end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal(
   "request::titlebars", function(c)
      -- buttons for the titlebar
      local buttons = gears.table.join(
         awful.button({ }, 1, function()
               c:emit_signal("request::activate", "titlebar", {raise = true})
               awful.mouse.client.move(c)
         end),
         awful.button({ }, 3, function()
               c:emit_signal("request::activate", "titlebar", {raise = true})
               awful.mouse.client.resize(c)
         end)
      )

      awful.titlebar(c) : setup {
         { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
         },
         { -- Middle
            { -- Title
               align  = "center",
               widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
         },
         { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
         },
         layout = wibox.layout.align.horizontal
                                }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
                         c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- KeyboardStates = {
--    Normal = "setxkbmap-dv-shifted",
--    FG = "setxkbmap-dv-shifted-fg",
-- }

-- KeyboardStateByClass = {
--    steam_app_1384160 = "FG",
--    steam_app_1364780 = "FG",
--    steam_app_1778820 = "FG",
--    steam_app_1959140 = "FG",
--    steam_app_2217000 = "FG",
--    steam_app_2109060 = "FG",
-- }

-- curr_kb_state = "Normal"

-- client.connect_signal(
--    "focus", function (c)
--       newstate = (c and c.class and KeyboardStateByClass[c.class:lower()]) or "Normal"
--       if newstate ~= curr_kb_state then
--          naughty.notify({ text = "KBState " .. newstate,
--                           timeout = 1})
--          awful.spawn(KeyboardStates[newstate])
--          curr_kb_state = newstate
--       end
-- end)
