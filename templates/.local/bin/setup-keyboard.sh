#!/usr/bin/env sh
# Those things get reset to the xorg config values occasionally.
# Hotplug and dpms seem to trigger it?
setxkbmap-dv-shifted || echo setxkbmap-dv-shifted: error $? 1>&2
# xkb-switch does not work with my config.
xkblayout-state set 1 || echo xkblayout-state: error $? 1>&2
# xset -r || echo xset: error $? 1>&2
