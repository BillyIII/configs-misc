#!/usr/bin/env sh

"`dirname \"$0\"`/mkconfig.sh" && ((i3-msg -t get_config | diff ~/.config/i3/config - 2>&1 >/dev/null) || i3-msg reload)
