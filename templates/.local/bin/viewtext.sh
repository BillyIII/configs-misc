#!/usr/bin/env sh

withtext "$1" emacsclient -e \''(progn (find-file-other-frame "{}") (my/setup-reading-buffer))'\'
