#!/usr/bin/env sh

set -u

SIG=
PFX_FILTER="*"

while getopts "ktp:" OPT; do
    case "$OPT" in
        k) SIG="9" ;;
        t) SIG="15" ;;
        p) PFX_FILTER="$OPTARG" ;;
        ?)
        printf "Usage: %s: [-k(SIGKILL)|-t(SIGTERM)] [-p PREFIX_FILTER]\n" $0
        exit 1
        ;;
    esac
done

shift $(($OPTIND - 1))

for p in $(find /proc -maxdepth 1 -type d); do
    PFX=$(cat $p/environ 2>/dev/null | tr '\0' '\n' | grep WINEPREFIX | sed sq^WINEPREFIX=qq)
    if [ -n "$PFX" ]; then
        case "$PFX" in ($PFX_FILTER)
            PID=$(echo $p | sed sq^/proc/qq)
            CMD=$(cat $p/cmdline | tr '\0' ' ' | sed 'sq *$qq' | sed 'sq\\q\\\\qg')

            if [ -z "$SIG" ]; then
                printf "$PID\t$PFX\t$CMD\n"
            else
                kill -"$SIG" "$PID"
            fi
        ;; esac
    fi
done
