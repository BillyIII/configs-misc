#-*- mode: sh -*-
# Set up the prompt

export fpath=("$HOME/.zsh.d" "$fpath[@]")

autoload -Uz promptinit
promptinit
prompt my

setopt histignorealldups sharehistory

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2

<% if [ $HAVE_GNU -gt 0 ]; then %>
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
<% fi %>

zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

<% if [ $HAVE_GNU -gt 0 ]; then %>
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='grep -F --color=auto'
alias egrep='grep -E --color=auto'
<% elif which colorls 2>&1 >/dev/null; then %>
alias ls='colorls -G'
<% fi %>

autoload -U select-word-style
select-word-style bash

zstyle ':zle:backward-kill-word*' word-style bash
# zstyle ':zle:backward-kill-word*' word-chars ${WORDCHARS//\/}

# This (completely separate from the above) extends word contexts
# so that "whitespace" is space betwen words, anything with
# a slash in is "filename", anything with a comma or equals is
# "list", the end of the line is "end", anything else is "other".
zstyle ':zle:*' word-context "[[:space:]]" whitespace "*/*" \
    filename "*[,=]*" list "" end "*" other
# Transposing words swaps shell words if you're between words.
zstyle ':zle:transpose-words:whitespace' word-style shell
# Transposing words only swaps alphanumeric characters when
# in a string with a /
zstyle ':zle:transpose-words:filename' word-style normal
zstyle ':zle:transpose-words:filename' word-chars ''
# Any of the kill functions use a reduced notion of word characters
# (i.e. stop killing on a larger set of characters) when in
# an argument that looks like a list.
zstyle ':zle:*kill-*:list' word-style normal
zstyle ':zle:*kill-*:list' word-chars '-_.~'

setopt no_auto_menu

[ -r "$HOME/.ghcup/env" ] && . "$HOME/.ghcup/env"

# tmux ignores PATH in update-environment?
# source the profile again...
. ~/.profile
