#!/usr/bin/env sh

# telegram: https://stackoverflow.com/a/77255128
# youtube: https://gist.github.com/mikoim/27e4e0dc64e384adbcb91ff10a2d3678


set -eu

TARGET=
FRAMERATE=
CHANNELS=
MUTE=
PLAY=
INFILE=
OUTFILE=
OUTFMT=
RUNPFX=exec
EXTRAARGS=
CLIPSTART=
CLIPDURATION=
CROPEXPR=
SCALEEXPR=
VIDEOFILTERS=
VERBOSE=


verbose_echo () {
    if [ "zyes" = "z$VERBOSE" ]; then
        echo "$@"
    fi
}

append_video_filter () {
    if [ "z" = "z$VIDEOFILTERS" ]; then
        VIDEOFILTERS="$1"
    else
        VIDEOFILTERS="$VIDEOFILTERS,$1"
    fi
}

exec_ffmpeg () {
    COMMONPFX=
    if [ "z" != "z$CLIPSTART" ]; then
        COMMONPFX="$COMMONPFX -ss $CLIPSTART"
    fi

    if [ "z" != "z$CROPEXPR" ]; then
        append_video_filter "crop=$CROPEXPR"
    fi
    if [ "z" != "z$SCALEEXPR" ]; then
        append_video_filter "scale=$SCALEEXPR"
    fi
    
    COMMONARGS=
    if [ "z" != "z$CLIPDURATION" ]; then
        COMMONARGS="$COMMONARGS -t $CLIPDURATION"
    fi
    if [ $CHANNELS -eq 0 ]; then
        COMMONARGS="$COMMONARGS -an"
    fi
    if [ "z" != "z$VIDEOFILTERS" ]; then
        COMMONARGS="$COMMONARGS -vf $VIDEOFILTERS"
    fi
    
    if [ "zyes" = "z$PLAY" ]; then
        $RUNPFX ffplay -hide_banner $COMMONPFX -i "$INFILE" $COMMONARGS
    else
        if [ "z" = "z$OUTFMT" ]; then
            echo "No output format"
            exit 2
        fi
        $RUNPFX ffmpeg -hide_banner $COMMONPFX -i "$INFILE" $COMMONARGS \
                "$@" $EXTRAARGS -f "$OUTFMT" "$OUTFILE"
    fi
}

ffprobe_number () {
    FFPROBE_NUMBER=$(ffprobe -v error -of default=noprint_wrappers=1:nokey=1 -select_streams "$1" \
                             -show_entries stream="$2" "$INFILE" | head -n1)
    if [ "z" = "z$FFPROBE_NUMBER" ]; then
        FFPROBE_NUMBER=0
    else
        FFPROBE_NUMBER=$(($FFPROBE_NUMBER))
    fi
    printf "%d" "$FFPROBE_NUMBER"
}

test_channels_list () {
    for CH in $*; do
        if [ $CHANNELS -eq $CH ]; then
            return 0
        fi
    done

    return 1
}

check_channels () {
    if ! test_channels_list "$@"; then
        echo "Invalid channels: " "$CHANNELS"
        exit 2
    fi
}

encode_telegram () {
    check_channels 0 1 2
    
    OUTFMT=mp4
    
    exec_ffmpeg -max_muxing_queue_size 9999 -c:v libx264 -crf 23 \
                -maxrate 4.5M -preset faster -flags +global_header -pix_fmt yuv420p \
                -profile:v baseline -movflags +faststart -c:a aac -profile:a aac_low
}

encode_youtube () {
    check_channels 0 1 2 6
    
    ABITRATE=
    case $CHANNELS in
         1) ABITRATE="-b:a 128k" ;;
         2) ABITRATE="-b:a 384k" ;;
         6) ABITRATE="-b:a 512k" ;;
    esac
    
    OUTFMT=mp4

    exec_ffmpeg -movflags +faststart -c:v libx264 -profile:v high \
                -bf 2 -g $(($FRAMERATE / 2)) -crf 18 -pix_fmt yuv420p -c:a aac \
                -profile:a aac_low $ABITRATE
}


while getopts "t:s:d:c:u:f:e:mpnvy" OPT; do
    case "$OPT" in
        t) TARGET="$OPTARG" ;;
        s) CLIPSTART="$OPTARG" ;;
        d) CLIPDURATION="$OPTARG" ;;
        c) CROPEXPR="$OPTARG" ;;
        u) SCALEEXPR="$OPTARG" ;;
        f) VIDEOFILTERS="$OPTARG" ;;
        e) EXTRAARGS="$EXTRAARGS $OPTARG" ;;
        m) MUTE="yes" ;;
        p) PLAY="yes" ;;
        n) RUNPFX="echo" ;;
        v) VERBOSE="yes" ;;
        y) EXTRAARGS="$EXTRAARGS -y" ;;
        ?)
        printf "Usage: %s: -t TARGET [-s CLIP_START] [-d CLIP_DURATION] [-c CROP_EXPRESSION] [-u SCALE_EXPRESSION] [-f VIDEO_FILTERS] [-e EXTRA_ARGS]* [-m (mute)] [-p (ffplay)] [-n (dry run)] [-v (verbose)] [-y (overwrite)] IN OUT\n" $0
        exit 1
        ;;
    esac
done

shift $(($OPTIND - 1))

INFILE="$1"

if [ ! "zyes" = "z$PLAY" ]; then
    OUTFILE="$2"
fi

FRAMERATE=$(ffprobe_number "v" "avg_frame_rate")
verbose_echo "Detected framerate: $FRAMERATE"

if [ "zyes" = "z$MUTE" ]; then
    CHANNELS=0
else
    
    CHANNELS=$(ffprobe_number "a" "channels")
fi
verbose_echo "Detected channels: $CHANNELS"

if [ "z" = "z$TARGET" ]; then
    echo "No target"
    exit 2
fi

case "$TARGET" in
    telegram) encode_telegram ;;
    youtube) encode_youtube ;;
    ?)
    echo "Unknown target: " "$TARGET"
    exit 1
    ;;
esac
