#!/usr/bin/env sh

set -eu

DESTDIR="$HOME/pictures/upload"

sxiv -fto "$@" | while read SRCNAME; do
    FNAME="$(basename "$SRCNAME")"
    FBASE="${FNAME%.*}"
    if [ "x" = "x$FBASE" ]; then
        FBASE="$FNAME"
    fi
    DESTNAME="$DESTDIR/$FBASE.webp"
    
    echo "From: $SRCNAME"
    echo "To:   $DESTNAME"
    mkdir -p "$DESTDIR"
    cwebp -m 6 -mt -af -short "$SRCNAME" -o "$DESTNAME"
    printf "%s" "$DESTNAME" | xclip -i -selection clipboard
done
