local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

local glib = require("lgi").GLib
local DateTime = glib.DateTime
local TimeZone = glib.TimeZone

local shortstatus = {}


function shortstatus.init()
   local statusbox = wibox({
         ontop = true,
         visible = false,
         x = 0,
         y = 0,
         width = 1,
         height = 1,
         opacity = 1.0,
         bg = beautiful.bg_normal,
         fg = beautiful.fg_normal,
         shape = gears.shape.round_rect,
         type = "toolbar"
   })

   statusbox:setup({
         {
            id = "text", 
            align = "left",
            font = beautiful.font,
            widget = wibox.widget.textbox
         },
         id = "margin",
         margins = beautiful.border_width,
         color = beautiful.border_focus,
         layout = wibox.container.margin,
   })

   awful.screen.connect_for_each_screen(function(s)
         s.statusbox = statusbox
   end)
end

local function trimstr(str)
   if not str then return nil end
   local r,_ = str:gsub("^%s*(.-)%s*$", "%1")
   return r
end

local function get_time()
   return DateTime.new_now(TimeZone.new_local()):format("%H:%M")
end

local function get_date()
   return DateTime.new_now(TimeZone.new_local()):format("%A, %e %B")
end

local function readfile(file)
   if not file then return nil end
   local text = file:read('*all')
   file:close()
   return text
end

local function readcmd(cmd)
   if not cmd then return nil end
   return readfile(io.popen(cmd, 'r'))
end

local obsdbatt = { "Disconnected", "Connected", "Backup" }

local function get_battery()
<% if [ `uname` = "Linux" ]; then %>
   local battery = "/sys/class/power_supply/BAT1/"
   local capacity = trimstr(readfile(io.open(battery .. "capacity")))
   local status = trimstr(readfile(io.open(battery .. "status")))
<% elif [ `uname` = "OpenBSD" ]; then %>
   local capacity = trimstr(readcmd('apm -l'))
   local status = tonumber(trimstr(readcmd('apm -a')))
   if status ~= nil and status < 3 then
      status = obsdbatt[status + 1]
   else
      status = nil
   end
<% else %>
   local capacity = nil
   local status = nil
<% fi %>

   if capacity ~= nil and status ~= nil then
      return capacity .. "%, " .. status
   elseif capacity ~= nil then
      return capacity .. "%"
   elseif status ~= nil then
      return status
   else
      return nil
   end
end

-- local function get_layout()
--    return fixstr(readfile(io.popen('xkb-switch', 'r')))
-- end

local function append_status(status, name, value)
   if value == nil then
      return status
   else
      line = name .. "\t<b>" .. value .. "</b>";
      if status:len() > 0 then
         return status .. "\n" .. line
      else
         return line
      end
   end
end

function shortstatus.show()
   local screen = mouse.screen;
   local box = screen.statusbox
   local margin = box:get_children_by_id("margin")[1]
   local text = box:get_children_by_id("text")[1]
   
   box.screen = screen

   local content = ""
   content = append_status(content, "Time", get_time())
   content = append_status(content, "Date", get_date())
   content = append_status(content, "Battery", get_battery())
   
   text:set_markup(content)

   local width, height = text:get_preferred_size(s)
   -- (*1.1): Width is not always correct. Avoid clipping on line wrap.
   box.width = width * 1.1 + margin.left + margin.right
   box.height = height + margin.top + margin.bottom
   awful.placement.align(
      box,
      {
         position = "top_right",
         honor_padding = true,
         honor_workarea = true,
      }
   )

   box.visible = true
end

function shortstatus.hide()
   screen[1].statusbox.visible = false
end



return shortstatus
