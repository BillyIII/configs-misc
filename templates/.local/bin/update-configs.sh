#!/usr/bin/env sh

~/src/configs-misc/templates-bin/do-templates.sh "$@" ~/src/configs-misc/templates ~/.home-template ~/
~/src/configs-misc/templates-bin/do-templates.sh "$@" ~/src/configs-misc-priv/templates ~/.home-template-priv ~/
