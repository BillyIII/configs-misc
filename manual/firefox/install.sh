#!/usr/bin/env sh

ROOT=`dirname "$0"`
ROOT=`realpath -s "$ROOT"`

set -eu


for PROFILE in $(find $HOME/.mozilla/firefox -mindepth 1 -maxdepth 1 -type d); do
    mkdir -p "$PROFILE/chrome"
    ln -sf "$ROOT/chrome/userChrome.css" "$PROFILE/chrome/"
    ln -sf "$ROOT/chrome/userChrome.css" "$PROFILE/chrome/userContent.css"
    ln -sf "$ROOT/user.js" "$PROFILE/user.js"
done
