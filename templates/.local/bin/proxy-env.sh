
# https://wiki.archlinux.org/title/Proxy_server#Environment_variables

assignProxy () {
    PROXY_VARS="http_proxy ftp_proxy https_proxy all_proxy HTTP_PROXY HTTPS_PROXY FTP_PROXY ALL_PROXY"
    NO_PROXY_VARS="no_proxy NO_PROXY"
    DEFAULT_NO_PROXY_LIST="127.*,192.168.*,10.*"
    
    NO_PROXY_LIST="${2-$DEFAULT_NO_PROXY_LIST}"
    
    for v in $PROXY_VARS; do
        export $v="$1"
    done
    
    for v in $NO_PROXY_VARS; do
        export $v="$NO_PROXY_LIST"
    done
}

clearProxy () {
    PROXY_ENV="http_proxy ftp_proxy https_proxy all_proxy HTTP_PROXY HTTPS_PROXY FTP_PROXY ALL_PROXY"
    NO_PROXY_VARS="no_proxy NO_PROXY"
    
    for v in $PROXY_VARS; do
        unset $v
    done
    
    for v in $NO_PROXY_VARS; do
        unset $v
    done
}
