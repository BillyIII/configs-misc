#!/usr/bin/env sh
set -eu
FROM="$1"
DURATION="$2"
INPUT="$3"
OUTPUT="$4"
exec ffmpeg -ss "$FROM" -i "$INPUT" -t "$DURATION" -vn "$OUTPUT"
