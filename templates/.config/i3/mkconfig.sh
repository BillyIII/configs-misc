#!/usr/bin/env sh

if ( cat ~/src/configs-misc/common/.config/i3/config_base \
     | ~/src/configs-misc/current/.config/i3/mkconfig.sh \
     > ~/.config/i3/config.tmp 2> ~/.config/i3/config.err \
   ); then
    mv ~/.config/i3/config.tmp ~/.config/i3/config
else
    exit $?
fi
