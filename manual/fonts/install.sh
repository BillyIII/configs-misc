#!/usr/bin/env sh

set -u

ROOT=$(dirname "$0")
ROOT=$(realpath "$ROOT")

DEST=${XDG_DATA_HOME:-"$HOME/.local/share"}/fonts

NAMES="EnvyCodeR AtkinsonHyperlegible IntelOneMono"


mkdir -p "$DEST"

for N in $NAMES; do
    for F in $ROOT/$N/*; do
        FNAME=$(basename "$F")
        ln -s "$F" "$DEST/$FNAME"
    done
done

if [ "`uname`" = "OpenBSD" ]; then
SYSFONTS=/usr/local/share/fonts
else
SYSFONTS=/usr/share/fonts
fi

# See fonts.conf for context.
delcyr() {
    DSTFONT=${2:-$(basename $1)}
    hb-subset --output-file=$HOME/.local/share/fonts/$DSTFONT '--unicodes=*' --unicodes-=0x0400-0x04FF "$SYSFONTS/$1"
}

if [ "`uname`" = "OpenBSD" ]; then
delcyr opendyslexic/OpenDyslexic-Regular.otf
delcyr opendyslexic/OpenDyslexic-Bold.otf
else
delcyr TTF/OpenDyslexic3-Regular.ttf
delcyr TTF/OpenDyslexic3-Bold.ttf
fi

fc-cache "$DEST"
